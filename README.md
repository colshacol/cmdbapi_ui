# cmdbapi_ui

[Video Preview](https://drive.google.com/file/d/0B4YUcg2HBSIvak03UzhZb3JVNG8/view)

### Features
- __Built from scratch with _React_.__ This means:
__1.__ No legacy code wrangling and inadequate documentation.  
__2.__ No head scratching "uh.. idk" moments. (i.e _hours_)  
__3.__ Familiarity and improved ability to reach deadlines.  

- __Nearly ZERO client-side configuration.__
__1.__ The UI structures itself from the ground up almost entirely  
based upon data received from the initial API request.
__2.__ Each nav link, view, table, table header, table row, etc is
dependent upon what the server tells the client it needs to display
to the user. All configured via JSON.
__3.__ Developers with no JavaScript/React/Front End experience can
scaffold out a new dashboard app to display whatever information they
desire simply by installing/cloning the app and designating the base
route for the API.

### ONE Line Client Configuration
In `./dist/index.html` you will find a script tag in the `head` that
contains one single global variable for the base API URL. All further
configuration can easily be done on the server in the language of
the developer's choice.

```
<script>
	// named to be globally safe.
	var $BASE_API_URL = 'http://localhost/api/v1'
</script>
```

### Opinionated Expectations
If I am not mistaken, this app needed to be able to be installed and
put into production accross infinite dynamic projects with the push
of a button. That said, I build it to be somewhat strict on the format
that it wants its data to arrive in. (Similar to `ng-admin`.) For our
current API to be valid for use with this app, either it or the
expectations for the received JSON structure must be changed.

_Note: everything is up for discussion/debate until production._

The current expected format from base API URL:
```
"views": [
	{
		// foo.bar/list/:client_path
		"client_path": string,

		// foo.bar/api/v1/:api_path
		"api_path": string,

		// title for nav and list view
		"title": string,

		// headers for list view table
		"table_headers": array[strings],

		// can users edit and save items to DB?
		"items_editable": boolean, // default: true

		// can users create new items in DB?
		"items_creatable": boolean, // default: true

		// can users export data from table into CSV?
		"data_exportable": boolean, // default: true
	},
	{ ... },
	{ ... }
]
```

For each object inside of the `views` array, the app will structure
out a link in the nav bar and prepare itself for receiving more
specific data when a user clicks on one of the links.

Upon clicking a nav link and navigating to the list view for the
particular item (i.e "devices" or "ips"), the app fires off a
request to the `api_path` specified above. The expected JSON response
looks like this:
```
"items": [
	{
		"foo": "oof",
		"bar": "rab",
		"baz": "zab"
	},
	{ ... },
	{ ... }
]
```

Each object in the `items` array will be represented as a row in the
table in the list view. The value associated with the key "foo", for
example, will be strategically placed in the column with the header
of "foo", as defined by the correlating object in the initial response.

### Plans:
1. Allow further building of the app, such as non list pages, footer and
nav links, etc, all through server provided JSON.
2. Integrate create/edit functionalities.
3. Add unit testing and type checking.

### Notes:
- Code is messy right now.
- Material design is integrated, but not fully.
