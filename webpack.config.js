const webpack = require('webpack')

module.exports = {
	context: `${__dirname}/src`,
	entry: './index.jsx',
	output: {
		path: `${__dirname}/dist/js`,
		filename: 'app.js'
	},

	resolve: {
		alias: {
			stores: `${__dirname}/src/stores`
		}
	},

	module: {
		loaders: [
			// babel-loader
			{
	      test: /(\.js|\.jsx)$/,
	      exclude: /(node_modules|bower_components)/,
	      loader: 'babel-loader',
	      query: {
	        presets: ['es2015', 'react'],
					plugins: ['transform-decorators-legacy', 'transform-class-properties', 'react-html-attrs']
	      }
			},

			// stylus-loader
			{
				test: /\.styl$/,
				loader: 'style-loader!css-loader!stylus-loader'
			},

			// svg-loader
			{
        test: /\.svg$/, loader: 'babel?presets[]=es2015,presets[]=react!svg-react'
      }
		]
	}
}