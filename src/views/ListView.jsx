import React from 'react'
import AppStore from 'stores/AppStore.jsx'
import DownloadIcon from '../images/download.svg'
import CreateIcon from '../images/create.svg'
import Header from '../comps/Header.jsx'

import './styles/ListView.styl'


const ListView = ({ params }) => {
	// Extract specific data needed for this view.
	const viewData = AppStore.views.filter(view => view.client_path === params.title)[0]

	// Generate specified table headers.
	const headers = viewData.table_headers.map((header, i) => (
		<th key={i}>
			{header.split('_').map(h => (
				h[0].toUpperCase() + h.substr(1)
			)).join(' ')}
		</th>
	))

	// Generate table rows from server provided data.
	const items = itemsData.map((item, i) => (
		<tr key={i}>
			{Object.keys(item).map((p, ii) => (
				<td key={ii}>{item[p]}</td>
			))}
		</tr>
	))

	const downloadTable = () => {
		const anchor = document.createElement('a')
		const csvString = document.querySelector('.content > table')
			.innerText.replace(/\t/g, '\,')

		anchor.download = 'x.csv'
		anchor.href = `data:application/csv;base64,${btoa(a)}`
		anchor.click()
		anchor.remove()
	}

	const insertNewRow = () => console.log('fix this!')

	return (
		<div class="list-view">
			<Header title={viewData.title}>
				<DownloadIcon onClick={downloadTable} />
				<CreateIcon class="create-icon" onClick={insertNewRow} />
			</Header>
			<div class="content">
				<table>
					<thead>
						<tr class="headers">
							{headers}
						</tr>
					</thead>
					<tbody>
						{items}
					</tbody>
				</table>
			</div>
		</div>
	)
}

export default ListView


const itemsData = [ // test data
	{"addresses": "a", "gateway": "1", "subnet": "2", "rack": "3", "vlan": "4", "equipment": "5", "type": "6", "description": "7"},
	{"addresses": "b", "gateway": "1", "subnet": "2", "rack": "3", "vlan": "4", "equipment": "5", "type": "6", "description": "7"},
	{"addresses": "c", "gateway": "1", "subnet": "2", "rack": "3", "vlan": "4", "equipment": "5", "type": "6", "description": "7"},
	{"addresses": "d", "gateway": "1", "subnet": "2", "rack": "3", "vlan": "4", "equipment": "5", "type": "6", "description": "7"},
	{"addresses": "e", "gateway": "1", "subnet": "2", "rack": "3", "vlan": "4", "equipment": "5", "type": "6", "description": "7"}
]

