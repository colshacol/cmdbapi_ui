import React from 'react'
import { Link } from 'react-router'
import axios from 'axios'
import MobxDevTools from 'mobx-react-devtools'
import AppStore from 'stores/AppStore.jsx'

import './styles/reset.styl'
import './App.styl'

const App = ({ children }) => {
	// For each view supplied by the initial API call,
	// create a nav link that directs the user to the
	// corresponding list view.
	const SideNavItems = AppStore.views.map((item, i) =>
		<li key={i}>
			<Link to={`/list/${item.client_path}`}>
				{item.title}
			</Link>
		</li>
	)

	return (
		<div id="app">
			<div id="side-nav">
				<h2>
					<a href="http://127.0.0.1:62415/">
						cmdb<span>api</span>
					</a>
				</h2>
				<ul>
					{SideNavItems}
				</ul>
			</div>

			<div id="views">
				{children}
			</div>
		</div>
	)
}

export default App