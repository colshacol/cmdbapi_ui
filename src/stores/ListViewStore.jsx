
export class ListViewStore {
	constructor(obj) {
		this.headers = obj._headers
		this.items = obj._items
		this.meta = obj._meta
	}
}