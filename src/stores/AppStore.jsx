
class AppStore {
	constructor(views) {
		this.views = api_v1["views"]
	}
}

const api_v1 = {
	"views": [
		{
			"client_path": "macs",
			"api_path": "macs",
			"title": "Mac Addresses",
			"table_headers": [
				"site_name", "site_code", "dev_type", "rack", "rack_unit", "equipment", "service_tag", "node_name", "cmn_network_ip", "location", "comment", "actions_stuff"
			]
		},
		{
			"client_path": "equipment",
			"api_path": "equipment",
			"title": "Equipment",
			"table_headers": [
				"name"
			]
		},
		{
			"client_path": "sites",
			"api_path": "sites",
			"title": "Sites",
			"table_headers": [
				"name", "site_code", "id", "location"
			]
		},
		{
			"client_path": "devices",
			"api_path": "devices",
			"title": "Devices",
			"table_headers": [
				"site_name", "site_code", "dev_type", "rack", "rack_unit", "equipment", "service_tag", "node_name", "cmn_network_ip", "location", "comment", "actions"
			]
		},
		{
			"client_path": "ips",
			"api_path": "ips",
			"title": "IPs",
			"table_headers": [
				"addresses", "gateway", "subnet", "rack", "vlan", "equipment", "type", "description"
			]
		},
		{
			"client_path": "logs",
			"api_path": "logs",
			"title": "Logs",
			"table_headers": [
				"site_name", "site_code", "dev_type", "rack", "rack_unit", "equipment", "service_tag", "node_name", "cmn_network_ip", "location", "comment", "actions"
			]
		},
		{
			"client_path": "vlans",
			"api_path": "vlans",
			"title": "Vlans",
			"table_headers": [
				"id", "name", "rack", "number"
			]
		}
	]
}


const store = new AppStore(api_v1["views"])
export default store