import React from 'react'
import ReactDOM from 'react-dom'
import { Link, browserHistory, Router, Route, IndexRoute } from 'react-router'

import App from './App.jsx'
import Dash from './views/Dash.jsx'
import ListView from './views/ListView.jsx'

ReactDOM.render(
	<Router history={browserHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={Dash} />
			<Route path="/list/:title" component={ListView} />
		</Route>
	</Router>,
	document.getElementById('root')
)