import React from 'react'
import './styles/Header.styl'

const Header = ({ title, children }) => {
	return (
		<div id="view-header">
			<h1>{title}</h1>
			<div class="view-options">
				{children}
			</div>
		</div>
	)
}

export default Header