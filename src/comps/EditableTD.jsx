import React from 'react'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'


const EditableTD = ({ tdData }) => {
	const state = observable({
		tdData,
		editable: false,
		get editingState() {
			console.log(this.editable)
			return this.editable
		}
	})

	const handleChange = action((newContent) => state.tdData = newContent)

	return (
		<td
			onDoubleClick={enableEditing}
		>{state.tdData}
		</td>
	)

}

export default observer(EditableTD)